package com.stolbunov.roman.data.repository.data_source;

import android.content.Context;
import android.content.SharedPreferences;

import com.stolbunov.roman.data.entity.LocaleNote;
import com.stolbunov.roman.data.repository.IDBNote;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DBNoteRepository implements IDBNote {
    private final String NAME_DB = "notes";
    private static final String KEY_NUMBER_NOTES = "number notes";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_PRIORITY = "priority";

    private static long counter = 0;

    private SharedPreferences preferences;

    public DBNoteRepository(Context context) {
        preferences = context.getSharedPreferences(NAME_DB, Context.MODE_PRIVATE);
    }

    @Override
    public boolean add(LocaleNote note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(KEY_NUMBER_NOTES, counter);
        editor.putString(KEY_TITLE + counter, note.getTitle());
        editor.putString(KEY_DESCRIPTION + counter, note.getDescription());
        editor.putString(KEY_PRIORITY + counter, note.getPriority());
        if (editor.commit()) {
            counter++;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(LocaleNote note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_TITLE + note.getId());
        editor.remove(KEY_DESCRIPTION + note.getId());
        editor.remove(KEY_PRIORITY + note.getId());
        return editor.commit();
    }

    @Override
    public boolean changed(LocaleNote note) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_TITLE + note.getId(), note.getTitle());
        editor.putString(KEY_DESCRIPTION + note.getId(), note.getDescription());
        editor.putString(KEY_PRIORITY + note.getId(), note.getPriority());
        return editor.commit();
    }

    @Override
    public Observable<List<LocaleNote>> getNoteList() {
        long dbSize = preferences.getLong(KEY_NUMBER_NOTES, -1);
        return Observable.just(dbSize)
                .filter(this::emptyDBFilter)
                .map(this::uploadNoteList);
    }

    private boolean emptyDBFilter(long dbSize) {
        if (dbSize < 0) {
            counter = 0;
            return false;
        } else {
            counter = ++dbSize;
            return true;
        }
    }

    private List<LocaleNote> uploadNoteList(long numberNotes) {
        List<LocaleNote> notes = new LinkedList<>();
        for (int i = 0; i < numberNotes; i++) {
            String title = preferences.getString(KEY_TITLE + i, KEY_TITLE);
            String description = preferences.getString(KEY_DESCRIPTION + i, KEY_DESCRIPTION);
            String priority = preferences.getString(KEY_PRIORITY + i, KEY_PRIORITY);

            if (!priority.equals(KEY_PRIORITY)) {
                notes.add(new LocaleNote(i, title, description, priority));
            }
        }
        return notes;
    }
}
