package com.stolbunov.roman.data.repository;

import android.content.Context;

import com.stolbunov.roman.data.entity.LocaleNote;
import com.stolbunov.roman.data.mapper.NoteMapper;
import com.stolbunov.roman.data.repository.data_source.DBNoteRepository;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.NoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.IUseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RepositoryManager implements NoteRepositoryBoundary {
    private DBNoteRepository db;

    public RepositoryManager(Context context) {
        db = new DBNoteRepository(context);
    }

    @Override
    public boolean add(Note note) {
        LocaleNote localeNote = NoteMapper.transform(note);
        return db.add(localeNote);
    }

    @Override
    public boolean remove(Note note) {
        LocaleNote localeNote = NoteMapper.transform(note);
        return db.remove(localeNote);
    }

    @Override
    public boolean change(Note note) {
        LocaleNote localeNote = NoteMapper.transform(note);
        return db.changed(localeNote);
    }

    @Override
    public Observable<List<Note>> uploadNoteList() {
        return db.getNoteList()
                .map(NoteMapper::transformList)
                .subscribeOn(Schedulers.io());
    }
}
