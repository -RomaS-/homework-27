package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;

import java.util.List;

import io.reactivex.Observable;

public interface NoteRepositoryBoundary {
    boolean add(Note note);

    boolean remove(Note note);

    boolean change(Note note);

    Observable<List<Note>> uploadNoteList();
}
