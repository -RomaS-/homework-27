package com.stolbunov.roman.domain.use_case.interactor;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.NoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.BaseUseCase;

import java.util.List;

import io.reactivex.Observable;

public class NoteListInteractor extends BaseUseCase {

    public NoteListInteractor(NoteRepositoryBoundary boundary) {
        super(boundary);
    }

    @Override
    public boolean add(Note note) {
        return boundary.add(note);
    }

    @Override
    public boolean remove(Note note) {
        return boundary.remove(note);
    }

    @Override
    public boolean change(Note note) {
        return boundary.change(note);
    }

    @Override
    public Observable<List<Note>> getNoteList() {
        return boundary.uploadNoteList();
    }
}
