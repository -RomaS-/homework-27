package com.stolbunov.roman.domain.use_case;

import com.stolbunov.roman.domain.repository.NoteRepositoryBoundary;

public abstract class BaseUseCase implements IUseCase {
    protected NoteRepositoryBoundary boundary;

    public BaseUseCase(NoteRepositoryBoundary boundary) {
        this.boundary = boundary;
    }
}
