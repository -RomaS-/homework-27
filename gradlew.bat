package com.stolbunov.roman.homework24.ui.screen;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;

import com.stolbunov.roman.homework24.R;
import com.stolbunov.roman.homework24.ui.fragment.FragmentDialog;
import com.stolbunov.roman.homework24.ui.fragment.InputDialogCallback;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements InputDialogCallback {
    public static final String FILE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatButton button = findViewById(R.id.btn_click_me);
        button.setOnClickListener(this::onStartClick);
    }

    private void onStartClick(View view) {
        FragmentDialog dialog = FragmentDialog.getInstance();
        dialog.show(getSupportFragmentManager(), "inputNumberDialog");
    }

    @Override
    public void onDialogButtonClick(int number) {
        SavingDataAsyncTask task = new SavingDataAsyncTask();
        task.execute(number);
    }

    class SavingDataAsyncTask extends AsyncTask<Integer, Void, File> {


        @Override
        protected File doInBackground(Integer... integers) {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(openFileOutput()))
            return null;
        }

        @Override
        protected void onPostExecute(File file) {
//            if (file != null && MainActivity.this.isAli)
            super.onPostExecute(file);
        }
    }



}
                                                                                                                                                                                                                                                                                                                                                                                                                                                       