package com.stolbunov.roman.homework_27.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.stolbunov.roman.domain.entity.Note;

public interface NoteEditorView extends MvpView {
    void showPriority(int priority);

    void editNote(Note note);
}
