package com.stolbunov.roman.homework_27.mvp.presenter;

import android.content.Context;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.stolbunov.roman.data.repository.RepositoryManager;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.domain.use_case.interactor.NoteListInteractor;
import com.stolbunov.roman.homework_27.R;
import com.stolbunov.roman.homework_27.mvp.view.NoteListView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class NoteListPresenter extends MvpPresenter<NoteListView> {
    private final String ERROR_MESSAGE = "There was a problem loading the data. Try again.";
    private IUseCase useCase;

    public void showDialog() {
        getViewState().showDialog();
    }

    public void hideDialog() {
        getViewState().hideDialog();
    }

    public void add(Note note) {
        if (useCase.add(note)) {
            getViewState().hideProgress();
            getViewState().add(note);
        } else {
            getViewState().showErrorSaveNoteMessage(ERROR_MESSAGE);
        }
    }

    public void remove(Note note) {
        if (useCase.remove(note)) {
            getViewState().remove(note);
        }else {
            getViewState().showErrorSaveNoteMessage(ERROR_MESSAGE);
        }
    }

    public void change(Note note) {
        if (useCase.change(note)) {
            getViewState().change(note);
        }else {
            getViewState().showErrorSaveNoteMessage(ERROR_MESSAGE);
        }
    }

    public void loadDataFromDB(Context context) {
        getViewState().showProgress();
        useCase = new NoteListInteractor(new RepositoryManager(context));
        Disposable subscribe = useCase.getNoteList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::complete, err -> errorHandling(context, err));
    }

    private void errorHandling(Context context, Throwable throwable) {
        getViewState().showErrorSaveNoteMessage(
                context.getString(R.string.error_load_message));
        Log.d(context.getString(R.string.tag), throwable.getMessage());
    }


    public void showErrorSaveMessage(String message) {
        getViewState().showErrorSaveNoteMessage(message);
    }

    public void editNote(Note note) {
        getViewState().goToEditor(note);
    }

    private void complete(List<Note> notes) {
        getViewState().hideProgress();
        getViewState().setDataAdapter(notes);
    }
}
