package com.stolbunov.roman.homework_27.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_27.mvp.view.NoteEditorView;

@InjectViewState
public class NoteEditorPresenter extends MvpPresenter<NoteEditorView> {

    public void showSelectedPriority(int priority) {
        getViewState().showPriority(priority);
    }

    public void editNote(Note note) {
        getViewState().editNote(note);
    }
}
